//
//  TableViewCell.swift
//  cinema_app
//
//  Created by Angel on 20.04.2019.
//  Copyright © 2019 Angel. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var filmName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
